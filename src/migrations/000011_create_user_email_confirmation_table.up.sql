CREATE TABLE IF NOT EXISTS user_email_confirmations (
    id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT UNSIGNED NOT NULL,
    email VARCHAR(100) NOT NULL,
    token VARCHAR(32) NOT NULL,
    status enum("pending", "confirmed", "expired") DEFAULT "pending",
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT fk_user_email_confirmations_user_id FOREIGN KEY (user_id) REFERENCES users(id)
) CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=INNODB;
CREATE UNIQUE INDEX user_email_confirmations ON user_email_confirmations(email) USING BTREE;