INSERT INTO financial_instrument_categories
    (name, type)
VALUES
    ('Mortgage', 'liability'),
    ('Other loans', 'liability'),
    ('Credit card debt', 'liability'),
    ('Other', 'liability');

INSERT INTO financial_instrument_categories
    (name, type)
VALUES
    ('Property', 'asset'),
    ('Automobile', 'asset'),
    ('Savings', 'asset'),
    ('Stock market shares', 'asset'),
    ('House contents (electronics, furniture, jewellery)', 'asset'),
    ('Other', 'asset');