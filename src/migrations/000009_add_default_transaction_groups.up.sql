
INSERT INTO transaction_categories 
	(slug, parent_slug, name, type, is_enabled) 
VALUES 
	('groceries', null, 'Groceries', 'expense', true),
	('transport', null, 'Transport', 'expense', true),
	('fuel', 'transport', 'Fuel', 'expense', true),
	('public-transport', 'transport', 'Public transport', 'expense', true),
	('laon', null, 'Loan payment', 'expense', true),
	('rates', null, 'Rates', 'expense', true),
	('rent', null, 'Rent', 'expense', true),
	('electronics', null, 'Electronics', 'expense', true),
	('other', null, 'Other', 'expense', true),
	('leasure', null, 'Leasure', 'expense', true),
	('restaurant', 'leasure', 'Restaurant', 'expense', true),
	('other', 'leasure', 'Other', 'expense', true),

	('salary', null, 'Salary', 'income', true),
	('government-payments', null, 'Government Payments', 'income', true),
	('interest', null, 'Interest', 'income', true),
	('stock-dividends', null, 'Stock dividends', 'income', true);
