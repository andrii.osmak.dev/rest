CREATE TABLE IF NOT EXISTS transaction_categories (
    id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    slug VARCHAR(50) NOT NULL,
    parent_slug VARCHAR(50) DEFAULT NULL,
    type ENUM('income', 'expense') NOT NULL DEFAULT 'expense',
    name VARCHAR(100) NOT NULL,
    is_enabled BOOLEAN DEFAULT false,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    INDEX (slug),
    UNIQUE KEY `idx_parent_id_slug` (`parent_slug`, `slug`),
    CONSTRAINT fk_ttransaction_categories_parent_slug FOREIGN KEY (parent_slug) REFERENCES transaction_categories(slug)
) CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=INNODB;
