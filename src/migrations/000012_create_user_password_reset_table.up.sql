CREATE TABLE IF NOT EXISTS user_password_resets (
    id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT UNSIGNED NOT NULL,
    token VARCHAR(32) NOT NULL,
    status enum("pending", "confirmed", "expired") DEFAULT "pending",
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT fk_user_password_resets_id FOREIGN KEY (user_id) REFERENCES users(id)
) CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=INNODB;