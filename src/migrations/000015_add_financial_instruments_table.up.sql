CREATE TABLE IF NOT EXISTS financial_instruments (
    id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT UNSIGNED NOT NULL,
    type enum("asset", "liability") NOT NULL,
    category_id BIGINT UNSIGNED NOT NULL,
    comment VARCHAR(100) DEFAULT NULL,
    amount FLOAT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT fk_financial_instruments_user_id FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT fk_financial_instruments_category_id FOREIGN KEY (category_id) REFERENCES financial_instrument_categories(id)
) CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=INNODB;
