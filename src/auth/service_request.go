package auth

import (
	"errors"
	"net/http"
)

type RequestService struct{}

func (service RequestService) GetUserInfoFromRequest(request *http.Request) (TokenUserInfo, error) {
	var err error
	var tokenUserInfo TokenUserInfo

	defaultError := errors.New("invalid token")

	claims := &Claims{}
	token, err := TokenService{}.getParsedTokenFromRequest(request, claims)

	if token == nil || err != nil {
		return tokenUserInfo, defaultError
	}

	return claims.TokenUserInfo, err
}
