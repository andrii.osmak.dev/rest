package auth

import (
	"net/http"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type TokenService struct{}

func (service TokenService) generateToken(user User, issuedAt time.Time, expiresIn time.Time) (string, error) {
	claims := &Claims{
		TokenUserInfo: TokenUserInfo{ID: user.Id},
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiresIn.Unix(),
			IssuedAt:  issuedAt.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign and get the complete encoded token as a string using the secret
	return token.SignedString(EnvService{}.getSecret())
}

func (service TokenService) parseToken(token string, claims *Claims) (*jwt.Token, error) {
	return jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return EnvService{}.getSecret(), nil
	})
}

func (service TokenService) getParsedTokenFromRequest(request *http.Request, claims *Claims) (*jwt.Token, error) {
	tokenString := strings.Replace(request.Header.Get("Authorization"), "Bearer ", "", -1)
	return TokenService{}.parseToken(tokenString, claims)
}
