package auth

import (
	"os"

	"github.com/joho/godotenv"
)

type EnvService struct{}

func (service EnvService) getSecret() []byte {
	godotenv.Load(".env")
	return []byte(os.Getenv("token_secret"))
}
