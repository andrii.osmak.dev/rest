package auth

import (
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type User struct {
	Id       uint64 `json:"id"`
	Email    string `json:"email"`
	Password string `json:"-"`
}

type loginSuccessResponseToken struct {
	Token     string    `json:"token"`
	IssuedAt  time.Time `json:"issued_at"`
	ExpiresAt time.Time `json:"expires_at"`
}

type loginSuccessResponse struct {
	AccessToken  loginSuccessResponseToken `json:"access_token"`
	RefreshToken loginSuccessResponseToken `json:"refresh_token"`
}

type Claims struct {
	TokenUserInfo TokenUserInfo `json:"user_info"`
	jwt.StandardClaims
}

type TokenUserInfo struct {
	ID uint64
}
