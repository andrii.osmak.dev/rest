package auth

import (
	"gitlab.com/andrii.osmak.dev/rest/src/db_client"
	"gorm.io/gorm"
)

var repositoryVar = &Repository{}

type Repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) {
	client := db
	repositoryVar = &Repository{
		db: client,
	}
}

func GetRepository() *Repository {
	if repositoryVar.db == nil {
		NewRepository(db_client.GetDbClient())
	}
	return repositoryVar
}

func (repository *Repository) findById(id uint64) (User, error) {
	var user User
	result := repository.db.First(&user, id)

	return user, result.Error
}

//@TODO refactor this method
func (repository *Repository) findByEmailAndPassword(email string, password string) (User, error) {
	var user User
	result := repository.db.Where("email = ? AND password = ?", email, password).First(&user)

	return user, result.Error
}
