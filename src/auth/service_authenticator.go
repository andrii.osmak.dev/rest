package auth

import (
	"errors"
	"net/http"
)

type AuthService struct{}

func (action AuthService) Authenticate(request *http.Request) error {
	var err error

	defaultError := errors.New("invalid token")
	claims := &Claims{}
	token, err := TokenService{}.getParsedTokenFromRequest(request, claims)

	if token == nil || err != nil || !token.Valid {
		return defaultError
	}

	return err
}
