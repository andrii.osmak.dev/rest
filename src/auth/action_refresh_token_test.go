package auth

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
)

func TestRefreshTokenActionNoToken(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{}`)
	request, err := http.NewRequest("GET", "/auth/token/refresh", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Invalid Token`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, RefreshTokenAction{}.Execute)
}

func TestRefreshTokenActionInvalidToken(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{}`)
	request, err := http.NewRequest("GET", "/auth/token/refresh", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", "invalid")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Invalid Token`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, RefreshTokenAction{}.Execute)
}

func TestRefreshTokenActionSuccess(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "email"}).
		AddRow(1, "test@email.com")

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `users")).WithArgs(1).WillReturnRows(rows)

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{}`)

	user := User{Id: 1}
	tokenIssuedAt := time.Now()
	accessTokenExpires := tokenIssuedAt.Add(15 * time.Minute)
	accessTokenString, _ := TokenService{}.generateToken(user, tokenIssuedAt, accessTokenExpires)

	request, err := http.NewRequest("GET", "/auth/token/refresh", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", "Bearer "+accessTokenString)
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		mock:         mock,
		expectedCode: http.StatusOK,
	}

	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(RefreshTokenAction{}.Execute)
	handler.ServeHTTP(responseRecorder, testObject.request)

	// we make sure that all expectations were met
	if err := testObject.mock.ExpectationsWereMet(); err != nil {
		testObject.test.Errorf("there were unfulfilled expectations: %s", err)
	}

	// Check the status code is what we expect.
	if status := responseRecorder.Code; status != testObject.expectedCode {
		testObject.test.Errorf("handler returned wrong status code: got %v want %v",
			status, testObject.expectedCode)
	}

	responseJson := responseRecorder.Body.String()
	var successResponse loginSuccessResponse
	err = json.Unmarshal([]byte(responseJson), &successResponse)

	if err != nil || successResponse.AccessToken.Token == "" || successResponse.RefreshToken.Token == "" {
		testObject.test.Errorf("Invalid response structure")
	}
}
