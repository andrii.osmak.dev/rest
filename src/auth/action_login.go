package auth

import (
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"gopkg.in/thedevsaddam/govalidator.v1"

	"gitlab.com/andrii.osmak.dev/rest/src/common"
)

type LoginAction struct{}

func (action LoginAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams loginRequestParams
	var user User
	var err error

	// request validation
	validationErrors := action.validateRequest(request, &requestParams)
	if len(validationErrors) > 0 {
		err := common.RequestValidationService{}.AddErrorsToResponse(validationErrors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// query user by email
	user, err = GetRepository().findByEmailAndPassword(requestParams.Email, common.HashGenerationService{}.GetHash(requestParams.Password))
	if err != nil {
		http.Error(responseWriter, "Unauthorized", http.StatusUnauthorized)
		return
	}

	// create new access token
	tokenIssuedAt := time.Now()
	accessTokenExpires := tokenIssuedAt.Add(15 * time.Minute)
	accessTokenString, err := TokenService{}.generateToken(user, tokenIssuedAt, accessTokenExpires)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// create new request token
	refreshTokenExpires := tokenIssuedAt.Add(24 * time.Hour)
	refreshTokenString, err := TokenService{}.generateToken(user, tokenIssuedAt, refreshTokenExpires)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// return tokens
	response := loginSuccessResponse{
		AccessToken: loginSuccessResponseToken{
			Token:     accessTokenString,
			IssuedAt:  tokenIssuedAt,
			ExpiresAt: accessTokenExpires,
		},
		RefreshToken: loginSuccessResponseToken{
			Token:     refreshTokenString,
			IssuedAt:  tokenIssuedAt,
			ExpiresAt: refreshTokenExpires,
		},
	}

	err = json.NewEncoder(responseWriter).Encode(response)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func (action LoginAction) validateRequest(request *http.Request, requestParams *loginRequestParams) url.Values {
	if request.Body == nil {
		return map[string][]string{"generic_errors": []string{"Invalid JSON or wrong parameter type"}}
	}

	rules := govalidator.MapData{
		"email":    []string{"required", "email"},
		"password": []string{"required", "min:6"},
	}

	// validate request
	opts := govalidator.Options{
		Request: request,
		Data:    requestParams,
		Rules:   rules,
	}

	validator := govalidator.New(opts)
	return validator.ValidateJSON()
}

type loginRequestParams struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
