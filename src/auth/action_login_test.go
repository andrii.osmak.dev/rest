package auth

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gorm.io/gorm"
)

func TestLoginActionValidationFailureMissingParams(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{}`)
	request, err := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `{"title":"Bad Request","errors":{"email":["The email field is required","The email field must be a valid email address"],"password":["The password field is required","The password field must be minimum 6 char"]}}`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, LoginAction{}.Execute)
}

func TestLoginActionValidationFailure(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"email": "notemail", "password": "short"}`)
	request, err := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(jsonStr))
	if err != nil {
		test.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/json")

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `{"title":"Bad Request","errors":{"email":["The email field must be a valid email address"],"password":["The password field must be minimum 6 char"]}}`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, LoginAction{}.Execute)
}

func TestLoginActionInvalidJson(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	request, err := http.NewRequest("POST", "/api/auth/login", nil)
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `{"title":"Bad Request","errors":{"generic_errors":["Invalid JSON or wrong parameter type"]}}`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, LoginAction{}.Execute)
}

func TestLoginActionInvalidCredentials(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `users`")).WithArgs("test@email.com", common.HashGenerationService{}.GetHash("secret")).WillReturnError(gorm.ErrRecordNotFound)

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"email": "test@email.com", "password": "secret"}`)
	request, err := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Unauthorized`,
		expectedCode: http.StatusUnauthorized,
	}

	runTest(testObject, LoginAction{}.Execute)
}

func TestLoginActionSuccess(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "email"}).
		AddRow(1, "test@email.com")

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `users")).WillReturnRows(rows)

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"email": "test@email.com", "password": "secret"}`)
	request, err := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		mock:         mock,
		expectedCode: http.StatusOK,
	}

	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(LoginAction{}.Execute)
	handler.ServeHTTP(responseRecorder, testObject.request)

	// we make sure that all expectations were met
	if err := testObject.mock.ExpectationsWereMet(); err != nil {
		testObject.test.Errorf("there were unfulfilled expectations: %s", err)
	}

	// Check the status code is what we expect.
	if status := responseRecorder.Code; status != testObject.expectedCode {
		testObject.test.Errorf("handler returned wrong status code: got %v want %v",
			status, testObject.expectedCode)
	}

	responseJson := responseRecorder.Body.String()
	var successResponse loginSuccessResponse
	err = json.Unmarshal([]byte(responseJson), &successResponse)

	if err != nil || successResponse.AccessToken.Token == "" || successResponse.RefreshToken.Token == "" {
		testObject.test.Errorf("Invalid response structure")
	}
}
