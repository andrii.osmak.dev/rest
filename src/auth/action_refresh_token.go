package auth

import (
	"encoding/json"
	"net/http"
	"time"
)

type RefreshTokenAction struct{}

func (action RefreshTokenAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var user User
	var err error

	userInfo, err := RequestService{}.GetUserInfoFromRequest(request)

	if err != nil {
		http.Error(responseWriter, "Invalid Token", http.StatusBadRequest)
		return
	}

	user, err = GetRepository().findById(userInfo.ID)

	if err != nil {
		http.Error(responseWriter, "Invalid token", http.StatusBadRequest)
		return
	}

	tokenIssuedAt := time.Now()
	accessTokenExpires := tokenIssuedAt.Add(15 * time.Minute)
	accessTokenString, err := TokenService{}.generateToken(user, tokenIssuedAt, accessTokenExpires)

	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	refreshTokenExpires := tokenIssuedAt.Add(24 * time.Hour)
	refreshTokenString, err := TokenService{}.generateToken(user, tokenIssuedAt, refreshTokenExpires)

	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	response := loginSuccessResponse{
		AccessToken: loginSuccessResponseToken{
			Token:     accessTokenString,
			IssuedAt:  tokenIssuedAt,
			ExpiresAt: accessTokenExpires,
		},
		RefreshToken: loginSuccessResponseToken{
			Token:     refreshTokenString,
			IssuedAt:  tokenIssuedAt,
			ExpiresAt: refreshTokenExpires,
		},
	}

	err = json.NewEncoder(responseWriter).Encode(response)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
