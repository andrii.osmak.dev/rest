package book

// Structs
type Book struct {
	Id       uint64  `json:"id" db:"id"`
	Isbn     string  `json:"isbn" db:"isbn"`
	Title    string  `json:"title" db:"title"`
	AuthorId uint64  `json:"-" db:"title"`
	Author   *Author `json:"author" gorm:"foreignKey:AuthorId"`
}

type Author struct {
	Id      uint64 `json:"id" db:"id"`
	Name    string `json:"name" db:"name"`
	Surname string `json:"surname" db:"surname"`
}

type upsertRequestParams struct {
	Isbn     string `json:"isbn" validate:"required"`
	Title    string `json:"title" validate:"required"`
	AuthorId uint64 `json:"author_id" validate:"required"`
}

type responseWitValidatinErrors struct {
	Title  string   `json:"title"`
	Errors []string `json:"errors"`
}
