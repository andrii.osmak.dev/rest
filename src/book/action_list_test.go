package book

import (
	"bytes"
	"net/http"
	"regexp"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
)

func TestListBooksActionSuccess(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "isbn", "title"}).
		AddRow(1, "12345", "test").
		AddRow(2, "65466", "test 2")

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `books")).WillReturnRows(rows)
	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"title": "test", "isbn": "12345", "author_id": 1}`)
	request, err := http.NewRequest("GET", "/api/books", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `[{"id":1,"isbn":"12345","title":"test","author":null},{"id":2,"isbn":"65466","title":"test 2","author":null}]`,
		expectedCode: http.StatusOK,
	}

	runTest(testObject, ListBooksAction)
}

func TestListBooksActionEmptyResult(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "isbn", "title"})

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `books")).WillReturnRows(rows)
	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"title": "test", "isbn": "12345", "author_id": 1}`)
	request, err := http.NewRequest("GET", "/api/books", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `[]`,
		expectedCode: http.StatusOK,
	}

	runTest(testObject, ListBooksAction)
}
