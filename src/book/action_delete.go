package book

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

func DeleteBookAction(responseWriter http.ResponseWriter, request *http.Request) {
	var bookId uint64
	var err error

	params := mux.Vars(request)
	bookId, err = strconv.ParseUint(params["id"], 10, 64)

	if err != nil {
		http.Error(responseWriter, "Bad Request", http.StatusBadRequest)
		return
	}

	err = GetRepository().delete(bookId)
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			http.Error(responseWriter, "Not Found", http.StatusNotFound)
			return
		}
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	responseWriter.WriteHeader(http.StatusAccepted)
}
