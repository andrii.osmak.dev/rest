package book

import (
	"database/sql"
	"net/http"
	"net/http/httptest"
	"testing"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type mockInterface interface {
	ExpectationsWereMet() error
}

type testObject struct {
	request      *http.Request
	test         *testing.T
	mock         mockInterface
	expectedBody string
	expectedCode int
}

func getGormDb(db *sql.DB) (*gorm.DB, error) {
	return gorm.Open(mysql.New(mysql.Config{
		SkipInitializeWithVersion: true,
		Conn:                      db,
	}), &gorm.Config{})
}

func runTest(testObject testObject, handlerFunc func(http.ResponseWriter, *http.Request)) {
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handlerFunc)
	handler.ServeHTTP(responseRecorder, testObject.request)

	// we make sure that all expectations were met
	if err := testObject.mock.ExpectationsWereMet(); err != nil {
		testObject.test.Errorf("there were unfulfilled expectations: %s", err)
	}

	// Check the status code is what we expect.
	if status := responseRecorder.Code; status != testObject.expectedCode {
		testObject.test.Errorf("handler returned wrong status code: got %v want %v",
			status, testObject.expectedCode)
	}
}
