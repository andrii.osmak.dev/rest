package book

import (
	"bytes"
	"net/http"
	"regexp"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

func TestGetBookActionSuccess(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "isbn", "title"}).
		AddRow(1, "12345", "test")

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `books")).WithArgs(1).WillReturnRows(rows)
	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"title": "test", "isbn": "12345", "author_id": 1}`)
	request, err := http.NewRequest("GET", "/api/books/1", bytes.NewBuffer(jsonStr))
	request = mux.SetURLVars(request, map[string]string{
		"id": "1",
	})
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `{"id":1,"isbn":"12345","title":"test","author":null}`,
		expectedCode: http.StatusOK,
	}

	runTest(testObject, GetBookAction)
}

func TestGetBookActionNotFound(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `books")).WithArgs(1).WillReturnError(gorm.ErrRecordNotFound)
	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"title": "test", "isbn": "12345", "author_id": 1}`)
	request, err := http.NewRequest("GET", "/api/books/1", bytes.NewBuffer(jsonStr))
	request = mux.SetURLVars(request, map[string]string{
		"id": "1",
	})
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Not Found`,
		expectedCode: http.StatusNotFound,
	}

	runTest(testObject, GetBookAction)
}

func TestGetBookActionBadRequest(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	request, err := http.NewRequest("GET", "/api/books/cheve", nil)
	if err != nil {
		test.Fatal(err)
	}
	request = mux.SetURLVars(request, map[string]string{
		"id": "cheve",
	})

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Bad Request`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, DeleteBookAction)
}
