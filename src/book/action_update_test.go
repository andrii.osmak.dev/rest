package book

import (
	"bytes"
	"net/http"
	"regexp"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/gorilla/mux"
)

func TestUpdateBookActionInvalidId(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{}`)
	request, err := http.NewRequest("PUT", "/api/books/test", bytes.NewBuffer(jsonStr))
	request = mux.SetURLVars(request, map[string]string{
		"id": "test",
	})
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Bad request`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, UpdateBookAction)
}

func TestUpdateBookActionValidationFailure(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{}`)
	request, err := http.NewRequest("PUT", "/api/books/1", bytes.NewBuffer(jsonStr))
	request = mux.SetURLVars(request, map[string]string{
		"id": "1",
	})
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `{"title":"Bad Request","errors":["Isbn is a required field","Title is a required field","AuthorId is a required field"]}`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, UpdateBookAction)
}

func TestUpdateBookActionBadRequest(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	request, err := http.NewRequest("PUT", "/api/books/1", nil)
	request = mux.SetURLVars(request, map[string]string{
		"id": "1",
	})
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Bad request`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, UpdateBookAction)
}

func TestUpdateBookActionInvalidJson(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{gfhfg}`)
	request, err := http.NewRequest("PUT", "/api/books/1", bytes.NewBuffer(jsonStr))
	request = mux.SetURLVars(request, map[string]string{
		"id": "1",
	})
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Invalid Json`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, UpdateBookAction)
}

func TestUpdateBookActionSuccess(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "isbn", "title"}).
		AddRow(1, "1234", "test")

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("UPDATE")).
		// WithArgs("1234", "test", 1).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `books")).WithArgs(1).WillReturnRows(rows)
	mock.ExpectCommit()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"title": "test", "isbn": "1234", "author_id": 1}`)
	request, err := http.NewRequest("PUT", "/api/books/1", bytes.NewBuffer(jsonStr))
	request = mux.SetURLVars(request, map[string]string{
		"id": "1",
	})
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `{"id":1,"isbn":"1234","title":"test","author":null}`,
		expectedCode: http.StatusOK,
	}

	runTest(testObject, UpdateBookAction)
}
