package book

import (
	"encoding/json"
	"net/http"
)

func ListBooksAction(responseWriter http.ResponseWriter, request *http.Request) {
	var books []Book
	var err error

	books, err = GetRepository().get()

	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	json.NewEncoder(responseWriter).Encode(books)
}
