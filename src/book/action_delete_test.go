package book

import (
	"net/http"
	"regexp"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/gorilla/mux"
)

func TestDeleteBookActionSuccess(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	// add expected queries
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("DELETE")).
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	request, err := http.NewRequest("DELETE", "/api/books/1", nil)
	if err != nil {
		test.Fatal(err)
	}
	request = mux.SetURLVars(request, map[string]string{
		"id": "1",
	})

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: ``,
		expectedCode: http.StatusAccepted,
	}

	runTest(testObject, DeleteBookAction)
}

func TestDeleteBookActionNotFound(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	// add expected queries
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("DELETE")).
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	request, err := http.NewRequest("DELETE", "/api/books/1", nil)
	if err != nil {
		test.Fatal(err)
	}
	request = mux.SetURLVars(request, map[string]string{
		"id": "1",
	})

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Not Found`,
		expectedCode: http.StatusNotFound,
	}

	runTest(testObject, DeleteBookAction)
}

func TestDeleteBookActionBadRequest(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	request, err := http.NewRequest("DELETE", "/api/books/cheve", nil)
	if err != nil {
		test.Fatal(err)
	}
	request = mux.SetURLVars(request, map[string]string{
		"id": "cheve",
	})

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Bad Request`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, DeleteBookAction)
}
