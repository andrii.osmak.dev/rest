package book

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gorm.io/gorm"

	"github.com/gorilla/mux"
)

func GetBookAction(responseWriter http.ResponseWriter, request *http.Request) {
	var book Book
	var bookId uint64
	var err error

	params := mux.Vars(request)
	bookId, err = strconv.ParseUint(params["id"], 10, 64)
	if err != nil {
		http.Error(responseWriter, "Bad Request", http.StatusBadRequest)
		return
	}

	book, err = GetRepository().findById(bookId)

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			http.Error(responseWriter, "Not Found", http.StatusNotFound)
			return
		}
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	json.NewEncoder(responseWriter).Encode(book)
}
