package book

import (
	"encoding/json"
	"net/http"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

func validateRequest(requestParams upsertRequestParams) []string {
	en := en.New()
	uni := ut.New(en, en)
	var validationErrors []string

	trans, _ := uni.GetTranslator("en")
	validate := validator.New()
	en_translations.RegisterDefaultTranslations(validate, trans)
	err := validate.Struct(&requestParams)
	if err != nil {
		errs := err.(validator.ValidationErrors)
		for _, e := range errs {
			validationErrors = append(validationErrors, e.Translate(trans))
		}
	}

	return validationErrors
}

func addErrorsToResponse(validationErrors []string, responseWriter http.ResponseWriter) error {
	errorsStruct := responseWitValidatinErrors{
		Title:  "Bad Request",
		Errors: validationErrors,
	}
	responseWriter.WriteHeader(http.StatusBadRequest)
	err := json.NewEncoder(responseWriter).Encode(errorsStruct)
	return err
}
