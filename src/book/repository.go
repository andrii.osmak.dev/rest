package book

import (
	"gitlab.com/andrii.osmak.dev/rest/src/db_client"
	"gorm.io/gorm"
)

var repositoryVar = &Repository{}

type Repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) {
	client := db
	repositoryVar = &Repository{
		db: client,
	}
}

func GetRepository() *Repository {
	if repositoryVar.db == nil {
		NewRepository(db_client.GetDbClient())
	}

	return repositoryVar
}

func (repository *Repository) delete(bookId uint64) error {
	result := repository.db.Delete(&Book{}, bookId)
	err := result.Error

	if err == nil && result.RowsAffected == 0 {
		err = gorm.ErrRecordNotFound
	}

	return err
}

func (repository *Repository) insert(book *Book) *gorm.DB {
	return repository.db.Create(&book)
}

func (repository *Repository) findById(id uint64) (Book, error) {
	var book Book
	result := repository.db.Preload("Author").First(&book, id)

	return book, result.Error
}

func (repository *Repository) get() ([]Book, error) {
	var books []Book
	result := repository.db.Preload("Author").Find(&books)

	return books, result.Error
}

func (repository *Repository) update(requestParams upsertRequestParams, book *Book) *gorm.DB {
	book.Isbn = requestParams.Isbn
	book.Title = requestParams.Title
	book.AuthorId = requestParams.AuthorId

	return repository.db.Save(&book)
}
