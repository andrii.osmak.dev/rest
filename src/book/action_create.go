package book

import (
	"encoding/json"
	"net/http"

	"gorm.io/gorm"

	"gitlab.com/andrii.osmak.dev/rest/src/common"
)

func CreateBookAction(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams upsertRequestParams
	var err error

	if request.Body != nil {
		err = json.NewDecoder(request.Body).Decode(&requestParams)
	}

	if request.Body == nil || err != nil {
		http.Error(responseWriter, "Invalid Json", http.StatusBadRequest)
		return
	}

	// request validation
	validationErrors := validateRequest(requestParams)
	if len(validationErrors) > 0 {
		requestValidationService := common.RequestValidationService{}
		requestValidationService.AddErrorsToResponseOld(validationErrors, responseWriter)
		return
	}

	// create a struct and insert in into the DB
	book := Book{Isbn: requestParams.Isbn, Title: requestParams.Title, AuthorId: requestParams.AuthorId}
	result := GetRepository().insert(&book)
	if result.Error != nil || result.RowsAffected == 0 {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// query newly created book
	book, err = GetRepository().findById(book.Id)

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			http.Error(responseWriter, "Not Found", http.StatusNotFound)
			return
		}
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	json.NewEncoder(responseWriter).Encode(book)
}
