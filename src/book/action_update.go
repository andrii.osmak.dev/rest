package book

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gorm.io/gorm"

	"github.com/gorilla/mux"
	"gitlab.com/andrii.osmak.dev/rest/src/common"
)

func UpdateBookAction(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams upsertRequestParams
	var bookId uint64
	var err error
	var book Book

	params := mux.Vars(request)
	bookId, err = strconv.ParseUint(params["id"], 10, 64)

	if bookId < 1 || request.Body == nil || err != nil {
		http.Error(responseWriter, "Bad request", http.StatusBadRequest)
		return
	}

	err = json.NewDecoder(request.Body).Decode(&requestParams)
	if err != nil {
		http.Error(responseWriter, "Invalid Json", http.StatusBadRequest)
		return
	}

	// request validation
	validationErrors := validateRequest(requestParams)
	if len(validationErrors) > 0 {
		requestValidationService := common.RequestValidationService{}
		err = requestValidationService.AddErrorsToResponseOld(validationErrors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// query newly created book
	book, err = GetRepository().findById(bookId)

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			http.Error(responseWriter, "Not Found", http.StatusNotFound)
			return
		}
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	result := GetRepository().update(requestParams, &book)

	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	json.NewEncoder(responseWriter).Encode(book)
}
