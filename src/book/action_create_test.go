package book

import (
	"bytes"
	"net/http"
	"regexp"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
)

func TestCreateBookActionValidationFailure(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{}`)
	request, err := http.NewRequest("POST", "/api/books", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `{"title":"Bad Request","errors":["Isbn is a required field","Title is a required field","AuthorId is a required field"]}`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, CreateBookAction)
}

func TestCreateBookActionInvalidJson(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	request, err := http.NewRequest("POST", "/api/books", nil)
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `Invalid Json`,
		expectedCode: http.StatusBadRequest,
	}

	runTest(testObject, CreateBookAction)
}

func TestCreateBookActionSuccess(test *testing.T) {
	db, mock, _ := sqlmock.New()

	mock.MatchExpectationsInOrder(false)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "isbn", "title"}).
		AddRow(1, "1234", "test")

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("INSERT")).
		WithArgs("1234", "test", 1).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `books")).WithArgs(1).WillReturnRows(rows)
	mock.ExpectCommit()

	gormDb, _ := getGormDb(db)

	// pass the mock into the repo
	NewRepository(gormDb)

	// create a request
	var jsonStr = []byte(`{"title": "test", "isbn": "1234", "author_id": 1}`)
	request, err := http.NewRequest("POST", "/api/books", bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		test.Fatal(err)
	}

	testObject := testObject{
		request:      request,
		test:         test,
		mock:         mock,
		expectedBody: `{"id":1,"isbn":"1234","title":"test","author":null}`,
		expectedCode: http.StatusOK,
	}

	runTest(testObject, CreateBookAction)
}
