package user

import (
	"encoding/json"
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
	"gorm.io/gorm"
)

type RegisterAction struct{}

func (registerAction RegisterAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams registrationRequestParams
	var err error

	// add validation rules
	//@todo add a regex for password validation
	rules := govalidator.MapData{
		"display_name":          []string{"required", "min:2"},
		"email":                 []string{"required", "email"},
		"password":              []string{"required", "min:6"},
		"password_confirmation": []string{"required", "min:6"},
	}

	// validate request
	opts := govalidator.Options{
		Request: request,
		Data:    &requestParams,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.ValidateJSON()
	requestValidationService := common.RequestValidationService{}
	if requestParams.Password != requestParams.PasswordConfirmation {
		errors = map[string][]string{"password": []string{"Password confirmation does not match"}}
	}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	repository := UserRepository{}
	repository.Init()

	// query user
	user, err := repository.findByEmail(requestParams.Email)
	if err == nil {
		//var validationErrors = []string{"The email address has already been taken"}
		errors = map[string][]string{"password": []string{"The email address has already been taken"}}
		err = requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	} else if err != nil && err != gorm.ErrRecordNotFound {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// create a user in the DB
	hashGenerationService := common.HashGenerationService{}
	user = User{
		DisplayName: requestParams.DisplayName,
		Email:       requestParams.Email,
		Password:    hashGenerationService.GetHash(requestParams.Password),
		Status:      "pending",
	}
	result := repository.DB.Create(&user)
	if result.Error != nil || result.RowsAffected == 0 {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// create a record in the user_email_confirmations table
	emailConfirmationToken := hashGenerationService.GetRandomString(32)
	userEmailConfirmation := UserEmailConfirmation{
		UserId: user.Id,
		Email:  user.Email,
		Token:  emailConfirmationToken,
		Status: "pending",
	}
	result = repository.DB.Create(&userEmailConfirmation)
	if result.Error != nil || result.RowsAffected == 0 {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// send a confirmation email
	sender := common.EmailService{}
	sender.SendEmail(common.EmailSettings{
		To:      user.Email,
		Subject: "Email verification",
		Body:    "Token: " + emailConfirmationToken,
	})

	responseWriter.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(responseWriter).Encode(user)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

type registrationRequestParams struct {
	DisplayName          string `json:"display_name" validate:"required,min=2"`
	Email                string `json:"email" validate:"required,email"`
	Password             string `json:"password" validate:"required,min=6"`
	PasswordConfirmation string `json:"password_confirmation" validate:"required,min=6"`
}
