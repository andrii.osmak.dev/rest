package user

type User struct {
	Id          uint64 `json:"-"`
	DisplayName string `json:"name"`
	Email       string `json:"email"`
	Password    string `json:"-"`
	Status      string `json:"-"`
}

type UserEmailConfirmation struct {
	Id     uint64
	Email  string
	UserId uint64
	Token  string
	Status string
}

type UserPasswordReset struct {
	Id     uint64
	UserId uint64
	Token  string
	Status string
}
