package user

import (
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
)

type ConfirmEmailAction struct{}

type emailResetRequestParams struct {
	Email string `json:"email"`
	Token string `json:"token"`
}

func (confirmEmailAction ConfirmEmailAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams emailResetRequestParams
	var err error

	// add validation rules
	//@todo add a regex for password validation
	rules := govalidator.MapData{
		"email": []string{"required", "email"},
		"token": []string{"required"},
	}

	// validate request
	opts := govalidator.Options{
		Request: request,
		Data:    &requestParams,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.ValidateJSON()
	requestValidationService := common.RequestValidationService{}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}
	// get user email confirmation
	confirmation := UserEmailConfirmation{}
	repository := UserRepository{}
	repository.Init()
	result := repository.DB.Where("email = ? AND token = ? AND status = ?", requestParams.Email, requestParams.Token, "pending").First(&confirmation)
	if result.Error != nil {
		http.Error(responseWriter, "Not Found", http.StatusNotFound)
		return
	}

	// update email confirmation
	confirmation.Status = "confirmed"
	result = repository.DB.Save(&confirmation)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// query user by id
	user, err := repository.findById(confirmation.UserId)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// update user's status
	user.Status = "active"
	result = repository.DB.Save(&user)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	http.Error(responseWriter, "", http.StatusNoContent)
}
