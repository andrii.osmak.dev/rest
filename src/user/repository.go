package user

import (
	"gitlab.com/andrii.osmak.dev/rest/src/common"
)

type UserRepository struct {
	common.BaseRepository
}

func (repository *UserRepository) findByEmail(email string) (User, error) {
	var user User
	result := repository.DB.Where("email = ?", email).First(&user)

	return user, result.Error
}

func (repository *UserRepository) findById(id uint64) (User, error) {
	var user User
	result := repository.DB.Where("id = ?", id).First(&user)

	return user, result.Error
}
