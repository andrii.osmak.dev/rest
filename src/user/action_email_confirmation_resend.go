package user

import (
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/auth"
	"gitlab.com/andrii.osmak.dev/rest/src/common"
)

type ConfirmEmailResendAction struct{}

func (confirmEmailResendAction ConfirmEmailResendAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	// get user data from the token
	tokenUserInfo, err := auth.RequestService{}.GetUserInfoFromRequest(request)
	if err != nil {
		http.Error(responseWriter, "Invalid token", http.StatusBadRequest)
		return
	}

	repository := UserRepository{}
	repository.Init()

	// query user email confirmation
	userEmailConfirmation := UserEmailConfirmation{}
	result := repository.DB.Where("user_id = ? and status = ?", tokenUserInfo.ID, "pending").First(&userEmailConfirmation)
	if result.Error != nil {
		http.Error(responseWriter, "User not found", http.StatusNotFound)
		return
	}

	// update use email confirmation
	emailConfirmationToken := common.HashGenerationService{}.GetRandomString(32)
	userEmailConfirmation.Token = emailConfirmationToken
	result = repository.DB.Save(&userEmailConfirmation)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// send an email
	sender := common.EmailService{}
	sender.SendEmail(common.EmailSettings{
		To:      userEmailConfirmation.Email,
		Subject: "Email verification",
		Body:    "Token: " + emailConfirmationToken,
	})

	http.Error(responseWriter, "", http.StatusAccepted)
}
