package user

import (
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
)

type PasswordResetRequestAction struct{}

func (passwordResetRequestAction PasswordResetRequestAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams passwordResetRequestParams
	var err error

	// add validation rules
	//@todo add a regex for password validation
	rules := govalidator.MapData{
		"email": []string{"required", "email"},
	}

	// validate request
	opts := govalidator.Options{
		Request: request,
		Data:    &requestParams,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.ValidateJSON()
	requestValidationService := common.RequestValidationService{}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	repository := UserRepository{}
	repository.Init()

	// query user
	user, err := repository.findByEmail(requestParams.Email)
	if err != nil {
		http.Error(responseWriter, "", http.StatusAccepted)
		return
	}

	// invalidate old password reset requests
	result := repository.DB.Model(UserPasswordReset{}).Where("user_id = ?", user.Id).Updates(UserPasswordReset{Status: "expired"})
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// add new password reset
	passwordResetToken := common.HashGenerationService{}.GetRandomString(32)
	passwordReset := UserPasswordReset{
		UserId: user.Id,
		Token:  passwordResetToken,
		Status: "pending",
	}
	result = repository.DB.Create(&passwordReset)
	if result.Error != nil || result.RowsAffected == 0 {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// send a confirmation email
	sender := common.EmailService{}
	sender.SendEmail(common.EmailSettings{
		To:      user.Email,
		Subject: "Password reset",
		Body:    "Token: " + passwordResetToken,
	})

	http.Error(responseWriter, "", http.StatusAccepted)
}

type passwordResetRequestParams struct {
	Email string `json:"email"`
}
