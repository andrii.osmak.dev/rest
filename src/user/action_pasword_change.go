package user

import (
	"encoding/json"
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/auth"
	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
)

type PasswordChangeAction struct{}

func (passwordChangeAction PasswordChangeAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams passwordChangeRequestParams
	var err error

	// get user data from the token
	tokenUserInfo, err := auth.RequestService{}.GetUserInfoFromRequest(request)
	if err != nil {
		http.Error(responseWriter, "Invalid token", http.StatusBadRequest)
		return
	}

	// add validation rules
	//@todo add a regex for password validation
	rules := govalidator.MapData{
		"old_password":              []string{"required", "min:6"},
		"new_password":              []string{"required", "min:6"},
		"new_password_confirmation": []string{"required", "min:6"},
	}

	// validate request
	opts := govalidator.Options{
		Request: request,
		Data:    &requestParams,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.ValidateJSON()
	requestValidationService := common.RequestValidationService{}
	if requestParams.NewPassword != requestParams.NewPasswordConfirmation {
		errors = map[string][]string{"password": []string{"Password confirmation does not match"}}
	}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	repository := UserRepository{}
	repository.Init()

	// query user
	user, err := repository.findById(tokenUserInfo.ID)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	hashGenerationService := common.HashGenerationService{}
	if hashGenerationService.GetHash(requestParams.OldPassword) != user.Password {
		errors = map[string][]string{"password": []string{"Invalid old password"}}
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// update user
	user.Password = hashGenerationService.GetHash(requestParams.NewPassword)
	result := repository.DB.Save(&user)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// return updated user
	err = json.NewEncoder(responseWriter).Encode(user)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

type passwordChangeRequestParams struct {
	OldPassword             string `json:"old_password"`
	NewPassword             string `json:"new_password"`
	NewPasswordConfirmation string `json:"new_password_confirmation"`
}
