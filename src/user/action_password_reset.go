package user

import (
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
	"gorm.io/gorm"
)

type PasswordResetAction struct{}

func (passwordResetAction PasswordResetAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams passwordResetParams
	var err error

	// add validation rules
	//@todo add a regex for password validation
	rules := govalidator.MapData{
		"token":                     []string{"required"},
		"new_password":              []string{"required", "min:6"},
		"new_password_confirmation": []string{"required", "min:6"},
	}

	// validate request
	opts := govalidator.Options{
		Request: request,
		Data:    &requestParams,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.ValidateJSON()
	requestValidationService := common.RequestValidationService{}
	if requestParams.NewPassword != requestParams.NewPasswordConfirmation {
		err := []string{"Password confirmation does not match"}
		errors = map[string][]string{"password": err}
	}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	repository := UserRepository{}
	repository.Init()

	// find password reset
	userPasswordReset := UserPasswordReset{}
	result := repository.DB.Where("token = ? AND status = ?", requestParams.Token, "pending").First(&userPasswordReset)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			http.Error(responseWriter, "Not Found", http.StatusNotFound)
			return
		}
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// query user by id
	user, err := repository.findById(userPasswordReset.UserId)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// update user
	hashGenerationService := common.HashGenerationService{}
	user.Password = hashGenerationService.GetHash(requestParams.NewPassword)
	result = repository.DB.Save(&user)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	userPasswordReset.Status = "confirmed"
	result = repository.DB.Save(&userPasswordReset)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

type passwordResetParams struct {
	Token                   string `json:"token"`
	NewPassword             string `json:"new_password"`
	NewPasswordConfirmation string `json:"new_password_confirmation"`
}
