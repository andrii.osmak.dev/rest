package user

import (
	"encoding/json"
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/auth"
	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
)

type UpdateProfileAction struct{}

func (updateProfileAction UpdateProfileAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var requestParams profileUpdateRequestParams
	var err error

	rules := govalidator.MapData{
		"display_name": []string{"required", "min:2"},
	}

	// validate request
	opts := govalidator.Options{
		Request: request,
		Data:    &requestParams,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.ValidateJSON()
	// requestValidationService := common.RequestValidationService{}
	if len(errors) > 0 {
		err := common.RequestValidationService{}.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// get user data from the token
	tokenUserInfo, err := auth.RequestService{}.GetUserInfoFromRequest(request)
	if err != nil {
		http.Error(responseWriter, "Invalid token", http.StatusBadRequest)
		return
	}

	repository := UserRepository{}
	repository.Init()

	// query user
	user, err := repository.findById(tokenUserInfo.ID)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// update user
	user.DisplayName = requestParams.DisplayName
	result := repository.DB.Save(&user)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// return updated user
	err = json.NewEncoder(responseWriter).Encode(user)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

type profileUpdateRequestParams struct {
	DisplayName string `json:"display_name"`
}
