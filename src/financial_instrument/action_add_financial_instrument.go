package financial_instrument

import (
	"encoding/json"
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/auth"
	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gorm.io/gorm"

	"gopkg.in/thedevsaddam/govalidator.v1"
)

type AddFinancialInsrumentsAction struct{}

func (action AddFinancialInsrumentsAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var financialInstrument newFinancialInstrument
	var financialInstrumentCategory financialInstrumentCategory

	rules := govalidator.MapData{
		"category_id": []string{"required", "numeric"},
		"type":        []string{"required", "in:asset,liability"},
		"amount":      []string{"required", "min:0", "float"},
	}

	opts := govalidator.Options{
		Request: request,
		Data:    &financialInstrument,
		Rules:   rules,
	}

	validator := govalidator.New(opts)
	errors := validator.ValidateJSON()
	requestValidationService := common.RequestValidationService{}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// get user data from the token
	tokenUserInfo, err := auth.RequestService{}.GetUserInfoFromRequest(request)
	if err != nil {
		http.Error(responseWriter, "Invalid token", http.StatusBadRequest)
		return
	}

	// initialise the repository
	repository := common.BaseRepository{}
	repository.Init()

	result := repository.
		DB.
		Where("id = ?", financialInstrument.CategoryId).
		Where("type = ?", financialInstrument.Type).
		First(&financialInstrumentCategory)

	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			http.Error(responseWriter, "Category does not exist", http.StatusNotFound)
			return
		}
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// set user ID
	financialInstrument.UserId = tokenUserInfo.ID

	// insert the financial instrument into the db
	result = repository.DB.Create(&financialInstrument)
	if result.Error != nil || result.RowsAffected == 0 {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// return the newly created financial instrument
	responseWriter.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(responseWriter).Encode(financialInstrument)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
