package financial_instrument

type FinancialInstrument struct {
	Id         uint64                       `json:"id"`
	UserId     uint64                       `json:"user_id"`
	CategoryId uint64                       `json:"category_id"`
	Type       string                       `json:"type""`
	Comment    string                       `json:"comment""`
	Amount     float32                      `json:"amount"`
	Category   *financialInstrumentCategory `json:"category" gorm:"foreignKey:CategoryId"`
}

type newFinancialInstrument struct {
	Id         uint64  `json:"id"`
	UserId     uint64  `json:"user_id"`
	CategoryId uint64  `json:"category_id"`
	Type       string  `json:"type"`
	Amount     float32 `json:"amount"`
	Comment    string  `json:"comment"`
}

func (newFinancialInstrument) TableName() string {
	return "financial_instruments"
}

type financialInstrumentCategory struct {
	Id   uint64 `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}
