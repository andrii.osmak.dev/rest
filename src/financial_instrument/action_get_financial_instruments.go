package financial_instrument

import (
	"encoding/json"
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/auth"
	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
	"gorm.io/gorm"
)

type GetFinancialInsrumentsAction struct{}

func (action GetFinancialInsrumentsAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var instruments []FinancialInstrument

	// request validation
	rules := govalidator.MapData{
		"category_id": []string{"numeric_between:1,"},
		"type":        []string{"in:asset,liability"},
	}

	opts := govalidator.Options{
		Request: request,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.Validate()
	if len(errors) > 0 {
		err := common.RequestValidationService{}.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// get user data from the token
	tokenUserInfo, err := auth.RequestService{}.GetUserInfoFromRequest(request)
	if err != nil {
		http.Error(responseWriter, "Invalid token", http.StatusBadRequest)
		return
	}

	// query user transactions
	query := action.getQuery(request, tokenUserInfo.ID)
	result := query.Find(&instruments)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// encode and return transactions
	err = json.NewEncoder(responseWriter).Encode(instruments)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func (action GetFinancialInsrumentsAction) getQuery(request *http.Request, userID uint64) *gorm.DB {
	repository := common.BaseRepository{}
	repository.Init()
	query := repository.DB

	// get the request params from the request
	requestParamsService := common.RequestParamsService{}
	typeParam, typeParamExists := requestParamsService.GetRequestParam(request, "type")
	if typeParamExists {
		query = query.Where("type = ?", typeParam)
	}

	cetegoryId, cetegoryIdExists := requestParamsService.GetRequestParam(request, "category_id")
	if cetegoryIdExists {
		query = query.Where("category_id = ?", cetegoryId)
	}

	return query.Preload("Category").Where("user_id = ?", userID)
}
