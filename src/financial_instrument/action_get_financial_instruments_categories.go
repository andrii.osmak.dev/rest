package financial_instrument

import (
	"encoding/json"
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
	"gorm.io/gorm"
)

type GetFinancialInsrumentsCategoriesAction struct{}

func (action GetFinancialInsrumentsCategoriesAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var result *gorm.DB
	var instrumentCategories []financialInstrumentCategory

	// request validation
	rules := govalidator.MapData{
		"type": []string{"required", "in:asset,liability"},
	}

	opts := govalidator.Options{
		Request: request,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.Validate()
	requestValidationService := common.RequestValidationService{}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// query results
	query := action.getQuery(request)
	result = query.Find(&instrumentCategories)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// encode and return the categories
	err := json.NewEncoder(responseWriter).Encode(instrumentCategories)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func (action GetFinancialInsrumentsCategoriesAction) getQuery(request *http.Request) *gorm.DB {
	repository := common.BaseRepository{}
	repository.Init()
	query := repository.DB

	// get the request params from the request
	requestParamsService := common.RequestParamsService{}

	typeParam, typeParamExists := requestParamsService.GetRequestParam(request, "type")
	if typeParamExists {
		query = query.Where("type = ?", typeParam)
	}

	return query
}
