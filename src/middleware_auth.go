package main

import (
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/auth"
)

// var authRequiredRoutes = map[string][]string{
// 	"/api/user/email/resend":                {"POST"},
// 	"/api/user":                             {"PUT"},
// 	"/api/user/password":                    {"PATCH"},
// 	"/api/transactions":                     {"GET", "POST"},
// 	"/api/transactions/categories":          {"GET"},
// 	"/api/financial/instruments":            {"GET", "POST"},
// 	"/api/financial/instruments/categories": {"GET"},
// }

func middlewareAuth(next http.HandlerFunc) http.HandlerFunc {
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		err := auth.AuthService{}.Authenticate(request)
		responseWriter.Header().Set("Content-type", "application/json")
		if err != nil {
			http.Error(responseWriter, "Unauthorized", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(responseWriter, request)
	}
}

func jsonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
		responseWriter.Header().Set("Content-type", "application/json")

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(responseWriter, request)
	})
}

// func authMiddleware(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
// 		uriString := strings.Split(request.RequestURI, "?")[0]
// 		responseWriter.Header().Set("Content-type", "application/json")
// 		methods, found := authRequiredRoutes[uriString]
// 		if found && contains(methods, request.Method) {
// 			err := auth.AuthService{}.Authenticate(request)
// 			if err != nil {
// 				http.Error(responseWriter, "Unauthorized", http.StatusUnauthorized)
// 				return
// 			}
// 		}

// 		// Call the next handler, which can be another middleware in the chain, or the final handler.
// 		next.ServeHTTP(responseWriter, request)
// 	})
// }

// func contains(s []string, str string) bool {
// 	for _, v := range s {
// 		if v == str {
// 			return true
// 		}
// 	}

// 	return false
// }
