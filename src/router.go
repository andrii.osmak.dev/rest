package main

import (
	"github.com/gorilla/mux"

	"gitlab.com/andrii.osmak.dev/rest/src/auth"
	// "gitlab.com/andrii.osmak.dev/rest/src/book"
	"gitlab.com/andrii.osmak.dev/rest/src/financial_instrument"
	"gitlab.com/andrii.osmak.dev/rest/src/transaction"
	"gitlab.com/andrii.osmak.dev/rest/src/user"
)

const routePrefix = "/api/"

func getRouter() *mux.Router {
	router := mux.NewRouter()

	// generic
	router.HandleFunc(routePrefix+"status", statusAction).Methods("GET")

	// // book
	// router.HandleFunc(routePrefix+"books", book.ListBooksAction).Methods("GET")
	// router.HandleFunc(routePrefix+"books/{id}", book.GetBookAction).Methods("GET")
	// router.HandleFunc(routePrefix+"books", book.CreateBookAction).Methods("POST")
	// router.HandleFunc(routePrefix+"books/{id}", book.DeleteBookAction).Methods("DELETE")
	// router.HandleFunc(routePrefix+"books/{id}", book.UpdateBookAction).Methods("PUT")

	//auth
	router.HandleFunc(routePrefix+"auth/login", auth.LoginAction{}.Execute).Methods("POST")
	router.HandleFunc(routePrefix+"auth/token/refresh", auth.RefreshTokenAction{}.Execute).Methods("POST")

	//user
	router.HandleFunc(routePrefix+"user", user.RegisterAction{}.Execute).Methods("POST")
	router.HandleFunc(routePrefix+"user/email/confirm", user.ConfirmEmailAction{}.Execute).Methods("PATCH")
	router.HandleFunc(routePrefix+"user/email/resend", middlewareAuth(user.ConfirmEmailResendAction{}.Execute)).Methods("POST")
	router.HandleFunc(routePrefix+"user", middlewareAuth(user.UpdateProfileAction{}.Execute)).Methods("PUT")
	router.HandleFunc(routePrefix+"user/password", middlewareAuth(user.PasswordChangeAction{}.Execute)).Methods("PATCH")
	router.HandleFunc(routePrefix+"user/password", user.PasswordResetRequestAction{}.Execute).Methods("POST")
	router.HandleFunc(routePrefix+"user/password", user.PasswordResetAction{}.Execute).Methods("PUT")

	//transaction
	router.HandleFunc(routePrefix+"transactions", middlewareAuth(transaction.GetTransactionsAction{}.Execute)).Methods("GET")
	router.HandleFunc(routePrefix+"transactions", middlewareAuth(transaction.AddTransactionAction{}.Execute)).Methods("POST")
	router.HandleFunc(routePrefix+"transactions/categories", middlewareAuth(transaction.GetTransactionCategoriesAction{}.Execute)).Methods("GET")

	//financial instruments
	router.HandleFunc(routePrefix+"financial/instruments", middlewareAuth(financial_instrument.GetFinancialInsrumentsAction{}.Execute)).Methods("GET")
	router.HandleFunc(routePrefix+"financial/instruments/categories", middlewareAuth(financial_instrument.GetFinancialInsrumentsCategoriesAction{}.Execute)).Methods("GET")
	router.HandleFunc(routePrefix+"financial/instruments", middlewareAuth(financial_instrument.AddFinancialInsrumentsAction{}.Execute)).Methods("POST")

	router.Use(jsonMiddleware)

	return router
}
