package common

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"math/rand"
	"time"
)

type HashGenerationService struct{}

func (service HashGenerationService) GetHash(text string) string {
	sha1Object := sha1.New()
	sha1Object.Write([]byte(text))
	return hex.EncodeToString(sha1Object.Sum(nil))
}

func (service HashGenerationService) GetRandomString(length int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, length)
	rand.Read(b)
	return fmt.Sprintf("%x", b)[:length]
}
