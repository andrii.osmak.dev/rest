package common

type EmailSettings struct {
	To      string
	Subject string
	Body    string
}
