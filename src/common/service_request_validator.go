package common

import (
	"encoding/json"
	"net/http"
	"net/url"
)

type RequestValidationService struct{}

func (requestValidationService RequestValidationService) AddErrorsToResponseOld(validationErrors []string, responseWriter http.ResponseWriter) error {
	errorsStruct := responseWitValidatinErrors{
		Title:  "Bad Request",
		Errors: validationErrors,
	}
	responseWriter.WriteHeader(http.StatusBadRequest)
	return json.NewEncoder(responseWriter).Encode(errorsStruct)
}

func (requestValidationService RequestValidationService) AddErrorsToResponse(validationErrors url.Values, responseWriter http.ResponseWriter) error {
	errorsStruct := responseWitValidatinErrorsNew{
		Title: "Bad Request",
	}

	if _, ok := validationErrors["_error"]; ok {
		err := []string{"Invalid JSON or wrong parameter type"}
		errorsStruct.Errors = map[string][]string{"generic_errors": err}
	} else {
		errorsStruct.Errors = validationErrors
	}

	responseWriter.WriteHeader(http.StatusBadRequest)
	return json.NewEncoder(responseWriter).Encode(errorsStruct)
}

type responseWitValidatinErrorsNew struct {
	Title  string     `json:"title"`
	Errors url.Values `json:"errors"`
}

type responseWitValidatinErrors struct {
	Title  string   `json:"title"`
	Errors []string `json:"errors"`
}
