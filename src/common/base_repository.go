package common

import (
	"gitlab.com/andrii.osmak.dev/rest/src/db_client"
	"gorm.io/gorm"
)

type BaseRepository struct {
	DB *gorm.DB
}

func (repository *BaseRepository) UpdateDBClient(dbClient *gorm.DB) {
	repository.DB = dbClient
}

func (repository *BaseRepository) Init() {
	if repository.DB == nil {
		repository.UpdateDBClient(db_client.GetDbClient())
	}
}
