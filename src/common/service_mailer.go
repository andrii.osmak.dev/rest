package common

import (
	"crypto/tls"
	"fmt"

	"os"
	"strconv"

	"github.com/joho/godotenv"
	gomail "gopkg.in/mail.v2"
)

type EmailService struct{}

func (service *EmailService) SendEmail(emailSettings EmailSettings) {
	mailConfig := service.getMailConfig()

	switch mailConfig.Driver {
	case "smtp":
		service.sendEmailSMTP(emailSettings)
	case "log":
		service.sendEmailLog(emailSettings)
	default:
		// do nothing
	}
}

func (service *EmailService) sendEmailLog(emailSettings EmailSettings) {
	//@todo add a proper logger here
	fmt.Println(emailSettings.To)
	fmt.Println(emailSettings.Subject)
	fmt.Println(emailSettings.Body)
}

func (service *EmailService) sendEmailSMTP(emailSettings EmailSettings) {

	m := gomail.NewMessage()

	smtpServerConfig := service.getSmtpServerConfig()

	// Set E-Mail sender
	m.SetHeader("From", smtpServerConfig.From)

	// Set E-Mail receivers
	m.SetHeader("To", emailSettings.To)

	// Set E-Mail subject
	m.SetHeader("Subject", emailSettings.Subject)

	// Set E-Mail body. You can set plain text or html with text/html
	m.SetBody("text/plain", emailSettings.Body)

	// Settings for SMTP server
	d := gomail.NewDialer(smtpServerConfig.Hostname, smtpServerConfig.Port, smtpServerConfig.Login, smtpServerConfig.Password)

	// This is only needed when SSL/TLS certificate is not valid on server.
	// In production this should be set to false.
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Now send E-Mail
	if err := d.DialAndSend(m); err != nil {
		fmt.Println(err)

		//@todo return an error instead
		panic(err)
	}

	fmt.Println("email has been sent")
}

func (service *EmailService) getSmtpServerConfig() smtpServerConfig {
	godotenv.Load(".env")
	portString := os.Getenv("MAIL_CLIENT_PORT")
	port, _ := strconv.Atoi(portString)

	return smtpServerConfig{
		Hostname: os.Getenv("MAIL_CLIENT_HOSTNAME"),
		Port:     port,
		Login:    os.Getenv("MAIL_CLIENT_LOGIN"),
		Password: os.Getenv("MAIL_CLIENT_PASSWORD"),
		From:     os.Getenv("MAIL_CLIENT_FROM"),
	}
}

func (service *EmailService) getMailConfig() mailConfig {
	godotenv.Load(".env")
	return mailConfig{
		From:   os.Getenv("MAIL_CLIENT_FROM"),
		Driver: os.Getenv("MAIL_DRIVER"),
	}
}

type smtpServerConfig struct {
	Hostname string
	Port     int
	Login    string
	Password string
	From     string
}

type mailConfig struct {
	From   string
	Driver string
}
