package common

import (
	"net/http"
)

type RequestParamsService struct{}

func (service RequestParamsService) GetRequestParam(request *http.Request, requestParamName string) (string, bool) {
	requestParam, ok := request.URL.Query()[requestParamName]
	if ok && len(requestParam) == 1 {
		return requestParam[0], true
	}

	return "", false
}
