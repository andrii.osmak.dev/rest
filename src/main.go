package main

import (
	"net/http"
	"os"

	_ "github.com/golang-migrate/migrate/v4"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
)

func main() {
	// get router with all the routes
	router := getRouter()

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{getEnvVar("ALLOWED_ORIGINS")},
		AllowCredentials: true,
		Debug:            true,
		AllowedHeaders:   []string{"*"},
	})

	handler := c.Handler(router)
	err := http.ListenAndServe(":8000", handler)
	if err != nil {
		panic(err.Error())
	}
}

func getEnvVar(varName string) string {
	godotenv.Load(".env")
	return os.Getenv(varName)
}
