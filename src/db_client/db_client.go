package db_client

import (
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	_ "github.com/go-sql-driver/mysql"
)

func GetDbClient() *gorm.DB {
	godotenv.Load(".env")
	db, err := gorm.Open(mysql.Open(os.Getenv("db_connect")), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	return db
}
