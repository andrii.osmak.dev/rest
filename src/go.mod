module gitlab.com/andrii.osmak.dev/rest/src

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/go-playground/validator/v10 v10.9.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang-jwt/jwt/v4 v4.1.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.15.0-beta.3.0.20210916234503-10a92d0cc819
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/rs/cors v1.8.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	golang.org/x/crypto v0.0.0-20210915214749-c084706c2272 // indirect
	golang.org/x/sys v0.0.0-20210915083310-ed5796bab164 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/mail.v2 v2.3.1 // indirect
	gopkg.in/thedevsaddam/govalidator.v1 v1.9.10
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.12
)
