package main

import (
	"encoding/json"
	"net/http"
)

func statusAction(responseWriter http.ResponseWriter, request *http.Request) {
	var response = map[string]string{
		"Status": "Ok",
	}

	err := json.NewEncoder(responseWriter).Encode(response)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
