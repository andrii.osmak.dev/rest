package transaction

type newTransaction struct {
	Id              uint64  `json:"id"`
	UserId          uint64  `json:"-"`
	Type            string  `json:"type"`
	CategoryId      uint64  `json:"category_id"`
	Amount          float32 `json:"amount"`
	Comment         string  `json:"comment"`
	TransactionDate string  `json:"date"`
}

func (newTransaction) TableName() string {
	return "transactions"
}

type Transaction struct {
	Id              uint64               `json:"id"`
	UserId          uint64               `json:"-"`
	Type            string               `json:"type"`
	CategoryId      uint64               `json:"-"`
	Amount          float32              `json:"amount"`
	Comment         string               `json:"comment"`
	TransactionDate string               `json:"date"`
	Category        *TransactionCategory `json:"category" gorm:"foreignKey:CategoryId"`
}

type TransactionCategory struct {
	Id         uint64 `json:"id"`
	Slug       string `json:"slug"`
	Type       string `json:"type"`
	ParentPlug string `json:"parent_slug"`
	Name       string `json:"name"`
}
