package transaction

import (
	"encoding/json"
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gopkg.in/thedevsaddam/govalidator.v1"
	"gorm.io/gorm"
)

type GetTransactionCategoriesAction struct{}

func (action GetTransactionCategoriesAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var result *gorm.DB
	var transactionCategories []TransactionCategory

	// request validation
	rules := govalidator.MapData{
		"parent": []string{"alpha_dash"},
		"type":   []string{"required", "in:income,expense"},
	}

	opts := govalidator.Options{
		Request: request,
		Rules:   rules,
	}

	v := govalidator.New(opts)
	errors := v.Validate()
	requestValidationService := common.RequestValidationService{}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// query results
	query := action.getQuery(request)
	result = query.Find(&transactionCategories)
	if result.Error != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// encode and return the categories
	err := json.NewEncoder(responseWriter).Encode(transactionCategories)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func (action GetTransactionCategoriesAction) getQuery(request *http.Request) *gorm.DB {
	repository := common.BaseRepository{}
	repository.Init()
	query := repository.DB

	// get the request params from the request
	requestParamsService := common.RequestParamsService{}
	parent, parentExists := requestParamsService.GetRequestParam(request, "parent")
	if !parentExists {
		// get all the parent categories
		query = query.Where("parent_slug IS NULL")
	} else {
		// get the child categories of the given patent category
		query = query.Where("parent_slug = ?", parent)
	}

	typeParam, typeParamExists := requestParamsService.GetRequestParam(request, "type")
	if typeParamExists {
		query = query.Where("type = ?", typeParam)
	}

	return query
}
