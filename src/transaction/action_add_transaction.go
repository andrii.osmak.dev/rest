package transaction

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/andrii.osmak.dev/rest/src/auth"
	"gitlab.com/andrii.osmak.dev/rest/src/common"
	"gorm.io/gorm"

	"gopkg.in/thedevsaddam/govalidator.v1"
)

type AddTransactionAction struct{}

func (action AddTransactionAction) Execute(responseWriter http.ResponseWriter, request *http.Request) {
	var transaction newTransaction
	var transactionCategory TransactionCategory

	rules := govalidator.MapData{
		"category_id": []string{"required", "numeric"},
		"type":        []string{"required", "in:income,expense"},
		"amount":      []string{"required", "min:0", "float"},
		"date":        []string{"required", "date:yyyy-mm-dd"},
		"comment":     []string{"alpha_space"},
	}

	messages := govalidator.MapData{
		"date": []string{"date:The date field must be in the following format: yyyy-mm-dd"},
	}

	opts := govalidator.Options{
		Request:  request,
		Data:     &transaction,
		Rules:    rules,
		Messages: messages,
	}

	validator := govalidator.New(opts)
	errors := validator.ValidateJSON()
	requestValidationService := common.RequestValidationService{}
	if len(errors) > 0 {
		err := requestValidationService.AddErrorsToResponse(errors, responseWriter)
		if err != nil {
			http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		}
		return
	}

	// get user data from the token
	tokenUserInfo, err := auth.RequestService{}.GetUserInfoFromRequest(request)
	if err != nil {
		http.Error(responseWriter, "Invalid token", http.StatusBadRequest)
		return
	}

	// initialise the repository
	repository := common.BaseRepository{}
	repository.Init()

	result := repository.
		DB.
		Where("id = ?", transaction.CategoryId).
		Where("type = ?", transaction.Type).
		First(&transactionCategory)

	if result.Error != nil {
		fmt.Println(err)
		if result.Error == gorm.ErrRecordNotFound {
			http.Error(responseWriter, "Category does not exist", http.StatusNotFound)
			return
		}
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// set user ID
	transaction.UserId = tokenUserInfo.ID

	// insert the transaction into the db
	result = repository.DB.Create(&transaction)
	if result.Error != nil || result.RowsAffected == 0 {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// return the newly created transaction
	responseWriter.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(responseWriter).Encode(transaction)
	if err != nil {
		http.Error(responseWriter, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
